# Alcommerce
Trabalho de AV3 da disciplina de Desenvolvimento Web 🚀

Como rodar o projeto:

1. Clone o repositório
2. Instalar as depêndencias do projeto com `npm install`
3. Rodar a aplicação com `npm start`

<hr>

Tecnologias envolvidas no projeto:

JS:
- ES6+
- React
- Redux
- redux-saga (facilita o controle dos 'side effects')
- Formatação de preço com a classe Intl
- immer (facilita o trabalho com os estados imutáveis)

CSS:
- Styled Components
- Animations CSS3
- React Content Loader
- react-toastify

Inspecionador do react (react-saga e redux):
- Reactotron (Aplicativo desktop)

<hr>
