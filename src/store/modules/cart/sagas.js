import {
  call, select, put, all, takeLatest,
} from 'redux-saga/effects';
import { toast } from 'react-toastify';
import api from '../../../services/api';
import history from '../../../services/history';
import { formatPrice } from '../../../util/format';
import { addToCartSuccess, updateAmountSuccess } from './actions';

function* addToCart({ id }) {
  const productExists = yield select((state) => state.cart.find((p) => p.id === id));

  const stock = yield call(api.get, `/products/${id}`);

  const stockAmount = stock.data.amount;
  const currentAmount = productExists ? productExists.amount : 0;

  const newAmount = currentAmount + 1;

  if (newAmount > stockAmount) {
    toast.error(`Esse produto possui apenas ${stockAmount} items em estoque.`, {
      toastId: productExists.id,
    });
    return;
  }

  if (productExists) {
    yield put(updateAmountSuccess(id, newAmount));
  } else {
    const response = yield call(api.get, `/products/${id}`);
    const data = {
      ...response.data.data,
      amount: 1,
      priceFormatted: formatPrice(response.data.data.price),
    };

    yield put(addToCartSuccess(data));

    history.push('/cart');
  }
}

function* updateAmount({ id, amount }) {
  if (amount <= 0) return;

  const stock = yield call(api.get, `/products/${id}`);
  const stockAmount = stock.data.quatityInStock;

  if (amount > stockAmount) {
    toast.error(`Esse produto possui apenas ${stockAmount} items em estoque.`, {
      toastId: 10,
    });
    return;
  }

  yield put(updateAmountSuccess(id, amount));
}

export default all([
  takeLatest('@cart/ADD_REQUEST', addToCart),
  takeLatest('@cart/UPDATE_AMOUNT_REQUEST', updateAmount),
]);
