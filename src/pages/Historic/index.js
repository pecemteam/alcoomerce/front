import React, { Component } from 'react';
import ContentLoader from 'react-content-loader';
import { ProductList } from './styles';

import { formatPrice } from '../../util/format';
import api from '../../services/api';
import Header from '../../components/Header';

class Historic extends Component {
  // eslint-disable-next-line react/state-in-constructor
  state = {
    historicList: [],
    isLoading: true,
  }

  async componentDidMount() {
    const token = localStorage.getItem('@Alcommerce:token');
    const user = JSON.parse(localStorage.getItem('@Alcommerce:user'));
    const { data } = await api.get(`clients/${user.id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    console.log('Here: ', data.data.purchases);

    this.setState({ historicList: data.data.purchases, isLoading: false });
  }

  formatDate = (date) => {
    const data = new Date(date);
    const dia = data.getDate().toString();
    const diaF = (dia.length === 1) ? `0${dia}` : dia;
    const mes = (data.getMonth() + 1).toString();
    const mesF = (mes.length === 1) ? `0${mes}` : mes;
    const anoF = data.getFullYear();
    return `${diaF}/${mesF}/${anoF}`;
  }

  render() {
    const { historicList, isLoading } = this.state;

    if (isLoading) {
      return (
        <ProductList>
          <li><ProductLoader /></li>
          <li><ProductLoader /></li>
          <li><ProductLoader /></li>
          <li><ProductLoader /></li>
          <li><ProductLoader /></li>
        </ProductList>
      );
    }
    return (
      <>
        <Header />
        <ProductList>
          {
            historicList.map((historic) => (
              <li key={historic.id}>
                <strong>
                  Data:
                  {' '}
                  {this.formatDate(historic.purchaseDate)}
                </strong>
                <span>
                  Valor:
                  {' '}
                  {formatPrice(historic.total)}
                </span>
              </li>
            ))
          }
        </ProductList>
      </>
    );
  }
}

const ProductLoader = () => (
  <ContentLoader
    height={530}
    width={400}
    speed={2}
    primaryColor="#f3f3f3"
    secondaryColor="#ecebeb"
  >
    <rect x="10" y="339" rx="4" ry="4" width="357" height="24" />
    <rect x="10" y="375" rx="4" ry="4" width="98" height="16" />
    <rect x="11" y="61" rx="5" ry="5" width="356" height="256" />
    <rect x="10" y="415" rx="4" ry="4" width="357" height="40" />
  </ContentLoader>
);

export default Historic;
