import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { MdAddShoppingCart } from 'react-icons/md';
import ContentLoader from 'react-content-loader';
import { ProductList } from './styles';
import { formatPrice } from '../../util/format';
import api from '../../services/api';
import * as CartActions from '../../store/modules/cart/actions';
import Header from '../../components/Header';

class Home extends Component {
  // eslint-disable-next-line react/state-in-constructor
  state = {
    products: [],
    isLoading: true,
  }

  async componentDidMount() {
    const token = localStorage.getItem('@Alcommerce:token');
    const { data } = await api.get('products', {
      params: {
        pagination: '{"page": 1, "perPage": 0 }',
        sort: ' {"field": "name", "order": "ASC" }',
        filter: '{"q":  "", "qField": "" }',
      },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const dataWithFormattedPrice = data.data.map((product) => ({
      ...product,
      formattedPrice: formatPrice(product.price),
    }));

    this.setState({ products: dataWithFormattedPrice, isLoading: false });
  }

  handleAddProduct = (id) => {
    // eslint-disable-next-line react/prop-types
    const { addToCartRequest } = this.props;
    addToCartRequest(id);
  }

  render() {
    const { products, isLoading } = this.state;
    // eslint-disable-next-line react/prop-types
    const { amount } = this.props;

    if (isLoading) {
      return (
        <ProductList>
          <li><ProductLoader /></li>
          <li><ProductLoader /></li>
          <li><ProductLoader /></li>
          <li><ProductLoader /></li>
          <li><ProductLoader /></li>
        </ProductList>
      );
    }
    return (
      <>
        <Header />
        <ProductList>
          {
            products.map((product) => (
              <li key={product.id}>
                <div>
                  <img src={`https://0d5968594830.ngrok.io/files/${product.photo[0]}`} alt={product.name} />
                </div>
                <strong>{product.name}</strong>
                <span>{product.formattedPrice}</span>

                <button type="button" onClick={() => this.handleAddProduct(product.id)}>
                  <div>
                    <MdAddShoppingCart size={16} color="#fff" />
                    {' '}
                    {amount[product.id] || 0}
                  </div>
                  <span>Adicionar ao carrinho</span>
                </button>
              </li>
            ))
          }
        </ProductList>
      </>
    );
  }
}

const ProductLoader = () => (
  <ContentLoader
    height={530}
    width={400}
    speed={2}
    primaryColor="#f3f3f3"
    secondaryColor="#ecebeb"
  >
    <rect x="10" y="339" rx="4" ry="4" width="357" height="24" />
    <rect x="10" y="375" rx="4" ry="4" width="98" height="16" />
    <rect x="11" y="61" rx="5" ry="5" width="356" height="256" />
    <rect x="10" y="415" rx="4" ry="4" width="357" height="40" />
  </ContentLoader>
);

const mapStateToProps = (state) => ({
  amount: state.cart.lenght > 0 ? state.cart.reduce((amount, product) => {
    // eslint-disable-next-line no-param-reassign
    amount[product.id] = product.amount;
    return amount;
  }, {}) : 0,
});

const mapDispatchToProps = (dispatch) => bindActionCreators(CartActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
