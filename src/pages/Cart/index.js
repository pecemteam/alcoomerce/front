/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { MdRemoveCircleOutline, MdAddCircleOutline, MdDelete } from 'react-icons/md';
import { toast } from 'react-toastify';
import { bindActionCreators } from 'redux';
import { Container, ProductTable, Total } from './styles';
import * as CartActions from '../../store/modules/cart/actions';
import { formatPrice } from '../../util/format';
import Header from '../../components/Header';
import api from '../../services/api';

function Cart({
  cart, total, totalInNumber, removeFromCart, updateAmountRequest, resetCart,
}) {
  function increment(product) {
    updateAmountRequest(product.id, product.amount + 1);
  }

  function decrement(product) {
    updateAmountRequest(product.id, product.amount - 1);
  }

  function handleSubmit() {
    const ids = [];
    cart.map((product) => {
      ids.push(product.id);
    });

    const user = JSON.parse(localStorage.getItem('@Alcommerce:user'));
    const token = localStorage.getItem('@Alcommerce:token');

    const data = {
      client: user.id,
      product: ids,
      total: totalInNumber,
    };

    api.post('purchases', data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).then(() => {
      resetCart();
    }).catch(() => {
      toast.error('Erro ao tentar realizar compra.', {
        toastId: 10,
      });
    });
  }

  return (
    <>
      <Header />
      <Container>
        <ProductTable>

          {!cart.length ? <h3>Seu carrinho está vazio..</h3> : ''}

          <thead>
            <tr>
              <th />
              <th>PRODUTO</th>
              <th>QTD</th>
              <th>SUBTOTAL</th>
              <th />
            </tr>
          </thead>

          <tbody>
            {cart.map((product) => (
              <tr key="product.id">
                <td>
                  <img src={`https://0d5968594830.ngrok.io/files/${product.photo[0]}`} alt={product.title} />
                </td>

                <td>
                  <strong>{product.name}</strong>
                  <span>{product.priceFormatted}</span>
                </td>

                <td>
                  <div className="box-button">
                    <button type="button" onClick={() => decrement(product)}>
                      <MdRemoveCircleOutline size={20} color="#f38c13" />
                    </button>

                    <input type="number" readOnly value={product.amount} />

                    <button type="button" onClick={() => increment(product)}>
                      <MdAddCircleOutline size={20} color="#f38c13" />
                    </button>
                  </div>
                </td>

                <td>
                  <strong>{product.subtotal}</strong>
                </td>

                <td>
                  <button type="button" onClick={() => removeFromCart(product.id)}>
                    <MdDelete size={20} color="#f38c13" />
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </ProductTable>

        <footer>
          <button type="button" onClick={handleSubmit}>Finalizar pedido</button>

          <Total>
            <span>TOTAL</span>
            <strong>{total}</strong>
          </Total>
        </footer>
      </Container>
    </>
  );
}

const mapStateToProps = (state) => ({
  cart: state.cart.length > 0 ? state.cart.map((product) => ({
    ...product,
    subtotal: formatPrice(product.price * product.amount),
  })) : [],

  total: state.cart.length > 0 ? formatPrice(state.cart.reduce((total, product) => total + product.price * product.amount, 0)) : formatPrice(0),
  totalInNumber: state.cart.length > 0 ? state.cart.reduce((total, product) => total + product.price * product.amount, 0) : 0,
});

const mapDispatchToProps = (dispatch) => bindActionCreators(CartActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
