import React from 'react';

import { AuthProvider } from './auth';
import { ToastProvider } from './toast';

// eslint-disable-next-line react/prop-types
const AppProvider = ({ children }) => (
  <AuthProvider>
    <ToastProvider>{children}</ToastProvider>
  </AuthProvider>
);

export default AppProvider;
