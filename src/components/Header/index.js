import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { MdShoppingBasket, MdTimer } from 'react-icons/md';
import { Container, Cart } from './styles';

import logoSrc from '../../assets/images/logo.png';

// eslint-disable-next-line react/prop-types
function Header({ cartSize }) {
  return (
    <Container>
      <Link to="/home">
        <img src={logoSrc} alt="Alcommerce" />
      </Link>

      <Cart to="/historic">
        <div className="cart-box">
          <strong>Meu histórico</strong>
        </div>
        <MdTimer size={36} color="#FFF" />
      </Cart>

      <Cart to="/cart">
        <div className="cart-box">
          <strong>Meu carrinho</strong>
          <span>
            {cartSize}
            {' '}
            itens
          </span>
        </div>
        <MdShoppingBasket size={36} color="#FFF" />
      </Cart>
    </Container>
  );
}

export default connect((state) => ({
  cartSize: state.cart.length,
}))(Header);
