import React from 'react';

import { Container } from './styles';

// eslint-disable-next-line react/prop-types
const Tooltip = ({ title, className, children }) => (
  <Container className={className}>
    {children}
    <span>{title}</span>
  </Container>
);

export default Tooltip;
