import React from 'react';

import { Container } from './styles';

// eslint-disable-next-line react/prop-types
const Button = ({ children, ...rest }) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <Container type="button" {...rest}>
    {children}
  </Container>
);

export default Button;
