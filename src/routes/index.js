import React from 'react';
import { Switch } from 'react-router-dom';

import Route from './Route';

import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';
import Forgot from '../pages/Forgot';

import Home from '../pages/Home';
import Historic from '../pages/Historic';
import Cart from '../pages/Cart';

const Routes = () => (
  <Switch>
    <Route path="/" exact component={SignIn} />
    <Route path="/signup" component={SignUp} />
    <Route path="/forgot" component={Forgot} />

    <Route path="/home" component={Home} isPrivate />
    <Route path="/historic" component={Historic} isPrivate />
    <Route path="/cart" component={Cart} isPrivate />
  </Switch>
);

export default Routes;
